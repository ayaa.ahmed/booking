<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href=" {{ asset('../../bootstrap/css/bootstrap.min.css') }} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/AdminLTE.min.css') }} ">
    <!-- Theme of profil page -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/profile.css') }} ">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/skins/_all-skins.min.css') }} ">

    <style>
        body {
            direction: rtl;
        }

    </style>
</head>

<body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        @include('layouts.header')
        <!-- Left side column. contains the sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    الملف الشخصي
                </h1><br>
                @include('layouts.messages')
            </section>

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">

                        <!-- Profile Image -->
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <img class="profile-user-img img-responsive img-circle"
                                    src=" {{ asset('../../dist/img/avatar.png') }}" alt="User profile picture">

                                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
                                <p class="profile-username text-center">{{ __('messages.Member') }}
                                    {{ Auth::user()->created_at->diffForHumans() }}</p>
                                <p class="profile-username text-center">الرصيد : {{ Auth::user()->credit }} </p>
                                <div class="col-md-12">
                                    <!-- general form elements -->
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">تغير كلمة المرور</h3>
                                        </div><!-- /.box-header -->
                                        <!-- form start -->
                                        <form method="POST" action="{{ route('change.password') }}">
                                            @csrf
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label for="password">كلمة السر الحالية</label>
                                                    <input id="password" type="password" class="form-control"
                                                        name="current_password" autocomplete="current-password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">كلمة السر</label>
                                                    <input id="new_password" type="password" class="form-control"
                                                        name="new_password" autocomplete="current-password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">تأكيد كلمة السر</label>
                                                    <input id="new_confirm_password" type="password"
                                                        class="form-control" name="new_confirm_password"
                                                        autocomplete="current-password">

                                                </div>
                                            </div><!-- /.box-body -->

                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-primary">تغير كلمة السر</button>
                                            </div>
                                        </form>
                                    </div><!-- /.box -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

            </section>
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>{{ __('messages.copy_rights') }} </strong>
    </footer>
    <div class="control-sidebar-bg"></div>
    </div>

    <!-- jQuery 2.1.4 -->
    <script src=" {{ asset('../../plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <!-- Bootstrap 3.3.4 -->
    <script src=" {{ asset('../../bootstrap/js/bootstrap.min.js') }} "></script>
    <!-- SlimScroll -->
    <script src=" {{ asset('../../plugins/slimScroll/jquery.slimscroll.min.js') }} "></script>
    <!-- FastClick -->
    <script src=" {{ asset('../../plugins/fastclick/fastclick.min.js') }} "></script>
    <script src=" {{ asset('../../dist/js/app.min.js') }} "></script>
    <!-- AdminLTE App -->
</body>

</html>
