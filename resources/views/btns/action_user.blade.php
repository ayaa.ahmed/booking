<div style="text-align: center">
    <a href="{{ url('/users/' . $id . '/edit') }}" class="btn btn-primary btn-sm"><abbr title="تعديل"><i
                class="fa fa-edit"></i></a></abbr>

    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
        data-target="#del_admin{{ $id }}"><abbr title="حذف"><i class="fa fa-trash"></i></button></abbr>

    @if ($status == 1 && $is_admin == '1')
        <abbr title="حذف"><button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                data-target="#ban_user{{ $id }}" disabled><i class="fa fa-ban"></i></button></abbr>
    @elseif( $status == 1)
        <abbr title="حظر"><button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                data-target="#ban_user{{ $id }}"><i class="fa fa-ban"></i></button></abbr>
    @else
        <abbr title="الغاءالحظر"><button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                data-target="#ban_user{{ $id }}"><i class="fa fa-unlock"></i></button></abbr>
    @endif

    <!-- Modal -->
    <div class="example-modal">
        <div id="del_admin{{ $id }}" class="modal modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            {{ __('messages.delete') }}
                        </h4>
                    </div>
                    <form action="{{ route('users.destroy', $id) }}" method="POST">
                        <div class="modal-body">
                            <h4>{{ __('messages.delete_user', ['name' => $name]) }}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left"
                                data-dismiss="modal">{{ __('messages.close') }}</button>
                            @csrf
                            @method('DELETE')
                            <button type="submit"
                                class="btn btn-outline pull-left">{{ __('messages.delete') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div>
    <div class="example-modal">
        <div id="ban_user{{ $id }}" class="modal modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            {{ __('messages.user-status') }}
                        </h4>
                    </div>
                    <form action="{{ url('status-update', $id) }}" method="get">
                        <div class="modal-body">
                            @if ($status == 1)
                                <h4>{{ __('messages.ban_this', ['name' => $name]) }}</h4>
                            @else
                                <h4>{{ __('messages.unban_this', ['name' => $name]) }}</h4>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left"
                                data-dismiss="modal">{{ __('messages.close') }}</button>
                            <button type="submit" class="btn btn-outline pull-left">{{ __('messages.ok') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
