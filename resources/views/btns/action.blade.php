<div style="text-align: center">
    <a href="{{ url('/booking/' . $id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>

    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
        data-target="#del_admin{{ $id }}"><i class="fa fa-trash"></i></button>

    <!-- Modal -->
    <div class="example-modal">
        <div id="del_admin{{ $id }}" class="modal modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">
                            {{ __('messages.delete') }}
                        </h4>
                    </div>
                    <form action="{{ route('booking.destroy', $id) }}" method="POST">
                        <div class="modal-body">
                            <h4>{{ __('messages.delete_this', ['name' => $firstname]) }}</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left"
                                data-dismiss="modal">{{ __('messages.close') }}</button>
                            @csrf
                            @method('DELETE')
                            <button type="submit"
                                class="btn btn-outline pull-left">{{ __('messages.delete') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
