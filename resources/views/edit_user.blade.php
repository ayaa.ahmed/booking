<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{ asset('../../bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('../../plugins/daterangepicker/daterangepicker-bs3.css') }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset('../../plugins/iCheck/all.css') }}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset('../../plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{ asset('../../plugins/timepicker/bootstrap-timepicker.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('../../plugins/select2/select2.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../../dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('../../dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../../dist/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../../dist/css/rtl.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">

        @include('layouts.header')
        <!-- Content Wrapper. Contains page content -->
        @include('layouts.sidebar')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{ __('messages.edit_user') }}
                    <small>{{ date('Y-m-d H:i:s') }}</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content"><br>
                @include('layouts.messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <!-- /.form group -->
                                <form action="{{ route('users.update', $user->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <!-- Date and time range -->
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>الاسم</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                <input name="name" value="{{ $user->name }}" type="text"
                                                    class="form-control" placeholder="الاسم" required>
                                            </div>

                                            <div class="form-group">
                                                <label>البريد اﻹليكتروني</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                    <input name="email" value="{{ $user->email }}" type="email"
                                                        class="form-control" placeholder="email@gmail.com">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>كلمة المرور</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                                    <input name="password" type="password" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>الرصيد</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-dollar"></i>
                                                    </div>
                                                    <input name="credit" value="{{ $user->credit }}" type="number"
                                                        class="form-control">
                                                </div><!-- /.input group -->
                                                <hr>
                                            </div>

                                            <h4>تعين كــ مشرف؟</h4>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <input type="radio" name="is_admin" value="1">
                                                        </span>
                                                        <input type="text" class="form-control" placeholder="نعم"
                                                            disabled>
                                                    </div><!-- /input-group -->
                                                </div><!-- /.col-lg-6 -->
                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <input type="radio" name="is_admin" value="0" checked>
                                                        </span>
                                                        <input type="text" class="form-control" placeholder="لا"
                                                            disabled>
                                                    </div><!-- /input-group -->
                                                </div><!-- /.col-lg-6 -->
                                            </div>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.box -->
                                    @foreach ($users->transactions as $trans)
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <label name="{{ $trans->id }}" class="form-control select2"
                                                style="width: 100%;" required>{{ $trans->name }}</label>
                                            <input name="price[]" value="{{ $trans->pivot->value }}" type="text"
                                                class="form-control" placeholder="السعر" required>
                                        </div><br>
                                    @endforeach
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary"
                                            style="width:100%">{{ __('messages.edit') }}</button>
                                    </div>
                                </form>
                            </div><!-- /.col (right) -->
                        </div><!-- /.row -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
            </div>
            <strong>{{ __('messages.copy_rights') }} </strong>
        </footer>

    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    <script src="{{ asset('../../plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <!-- Bootstrap 3.3.4 -->
    <script src="{{ asset('../../bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('../../plugins/select2/select2.full.min.js') }}"></script>
    <!-- InputMask -->
    <script src="{{ asset('../../plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ asset('../../plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset('../../plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{ asset('../../plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset('../../plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('../../plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{ asset('../../plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('../../plugins/iCheck/icheck.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('../../plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('../../dist/js/app.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('../../dist/js/demo.js') }}"></script>
</body>

</html>
