<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title> {{ $title }} </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href=" {{ asset('../../bootstrap/css/bootstrap.min.css') }} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/datatables/dataTables.bootstrap.css') }} ">
    <!-- Theme style -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/AdminLTE.min.css') }} ">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/skins/_all-skins.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('../../dist/css/bootstrap-rtl.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('../../dist/css/rtl.css') }} ">
    <script src=" {{ asset('../../dist/js/myFunctions.js') }} "></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }} "></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }} "></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">

        @include('layouts.header')
        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    {{ __('messages.Members') }}

                    <small>{{ date('Y-m-d H:i:s') }}</small>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header"><br>
                            </div><!-- /.box-header -->
                            @include('layouts.messages')
                            <form id="form_data" action="{{ url('/destroy/all/users') }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <div class="box-body">
                                    {!! $dataTable->table(['class' => 'table table-bordered table-hover'], true) !!}
                            </form>

                        </div><!-- /.box-body -->
                        <!-- Modal -->
                        <div class="example-modal">
                            <div id="multipleDelete" class="modal modal-danger">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">
                                                {{ __('messages.delete') }}
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="empty_record hidden">
                                                <p>{{ __('messages.please_check_some_records') }} ! </p>
                                            </div>
                                            <div class="not_empty_record hidden">
                                                <p>{{ __('messages.ask_delete_itme') }} <span
                                                        class="record_count"></span> ؟ </p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="empty_record hidden">
                                                <button type="button" class="btn btn-outline pull-left"
                                                    data-dismiss="modal">{{ __('messages.close') }}</button>
                                            </div>
                                            <div class="not_empty_record hidden">
                                                <button type="button" class="btn btn-outline pull-left"
                                                    data-dismiss="modal">{{ __('messages.close') }}</button>
                                                <button name="del_all" type="button"
                                                    class="btn btn-outline del_all">{{ __('messages.delete') }}</button>
                                            </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>

                    </div><!-- /.box -->
                </div><!-- /.col -->
        </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
        <strong>{{ __('messages.copy_rights') }} </strong>
    </footer>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src=" {{ asset('../../plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <!-- Bootstrap 3.3.4 -->
    <script src=" {{ asset('../../bootstrap/js/bootstrap.min.js') }} "></script>
    <!-- DataTables -->
    <script src=" {{ asset('../../plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src=" {{ asset('../../plugins/datatables/dataTables.bootstrap.min.js') }} "></script>
    <!-- SlimScroll -->
    <script src=" {{ asset('../../plugins/slimScroll/jquery.slimscroll.min.js') }} "></script>
    <!-- FastClick -->
    <script src=" {{ asset('../../plugins/fastclick/fastclick.min.js') }} "></script>
    <!-- AdminLTE App -->
    <script src=" {{ asset('../../dist/js/app.min.js') }} "></script>

    <!-- AdminLTE for demo purposes -->
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('/vendor/datatables/buttons.server-side.js') }}"></script>

    <!-- page script -->
    <script>
        delete_all()
    </script>
    {!! $dataTable->scripts() !!}
</body>

</html>
