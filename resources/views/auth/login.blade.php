<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>{{ __('messages.login') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href="{{ asset('../../bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../../dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('../../plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('../../dist/css/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../../dist/css/rtl.css') }}">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
        </div><!-- /.login-logo -->
        <div class="login-box-body"><br>
            <h3 class="login-box-msg">{{ __('messages.sign.in') }}</h3>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                @include('layouts.messages')
                <div class="form-group has-feedback">
                    <input id="email" type="email" class="form-control" @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                        placeholder="البريد اﻹلكتروني">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password"
                        placeholder="{{ __('messages.password') }}"">

                    @error('password') <span class="







                             <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <span class="glyphicon
                            glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-7">
                            <div class="checkbox icheck">
                                <label>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}> {{ __('messages.remember') }}
                                </label>
                            </div>
                        </div><!-- /.col -->
                        <div class="col-xs-5">
                            <button type="submit" class="btn btn-primary btn-flat">{{ __('messages.sign.in') }}</button>
                        </div><!-- /.col -->
                    </div>
                </form>

                @if (Route::has('password.request'))
                    <a href="{{ route('password.request') }}">{{ __('messages.forgot_password') }}</a><br><br>
                @endif

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

        <!-- jQuery 2.1.4 -->
        <script src="{{ asset('../../plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
        <!-- Bootstrap 3.3.4 -->
        <script src="{{ asset('../../bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ asset('../../plugins/iCheck/icheck.min.js') }}"></script>
        <script>
            $(function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>

    </html>
