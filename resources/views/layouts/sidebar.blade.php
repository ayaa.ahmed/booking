<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('../../dist/img/avatar.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> {{ __('messages.online') }}</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{ __('messages.main.menu') }}</li>
            <li><a href="{{ route('booking.index') }}"><i class="fa fa-home"></i>
                    <span>{{ __('messages.dashboard') }}</span></a></li>
            <li><a href="{{ route('booking.create') }}"><i class="fa fa-calendar-plus-o"></i>
                    <span>{{ __('messages.create_book') }}</span></a>
            </li>
            <li><a href="{{ route('change.password') }}"><i class="fa  fa-user"></i>
                    <span>{{ __('messages.profile') }}</span></a></li>
            @if (Auth::user()->is_admin == 1)
                <li class="header">{{ __('messages.admin.menu') }}</li>
                <li><a href="{{ route('users.index') }}"><i class="fa fa-group"></i>
                        <span>{{ __('messages.Members') }}</span></a></li>
                <li><a href="{{ route('create/transaction') }}"><i class="fa  fa-money"></i>
                        <span>{{ __('messages.create_trans') }}</span></a></li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
