<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link rel="stylesheet" href=" {{ asset('../../bootstrap/css/bootstrap.min.css') }} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/daterangepicker/daterangepicker-bs3.css') }} ">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/iCheck/all.css') }} ">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/colorpicker/bootstrap-colorpicker.min.css') }} ">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/timepicker/bootstrap-timepicker.min.css') }} ">
    <!-- Select2 -->
    <link rel="stylesheet" href=" {{ asset('../../plugins/select2/select2.min.css') }} ">
    <!-- Theme style -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/AdminLTE.min.css') }} ">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href=" {{ asset('../../dist/css/skins/_all-skins.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('../../dist/css/bootstrap-rtl.min.css') }} ">
    <link rel="stylesheet" href=" {{ asset('../../dist/css/rtl.css') }} ">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }} "></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }} "></script>
    <![endif]-->
</head>

<body class="skin-blue sidebar-mini" dir="rtl">
    <div class="wrapper">

        @include('layouts.header')
        <!-- Content Wrapper. Contains page content -->
        @include('layouts.sidebar')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    تسجيل طلب حجز موعد
                    <small>{{ date('Y-m-d H:i:s') }}</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content"><br>
                @include('layouts.messages')
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body">
                                <!-- /.form group -->
                                <form action="{{ route('booking.store') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group"><br>
                                        <label>نوع المعاملة</label>
                                        <select name="transaction" class="form-control select2" style="width: 100%;"
                                            required>
                                            <option value="{{ old('transaction') }}"></option>
                                            @foreach ($transactions as $trans)
                                                <option value="{{ $trans->id }}">{{ $trans->name }}</option>
                                            @endforeach

                                        </select>
                                    </div><!-- /.form-group -->
                                    <!-- Date and time range -->
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <input name="firstname" type="text" class="form-control"
                                                    placeholder="الاسم الأول" required
                                                    value="{{ old('firstname') }}">
                                            </div>
                                            <div class="col-xs-3">
                                                <input name="fathername" type="text" class="form-control"
                                                    placeholder="اسم اﻷب" required value="{{ old('fathername') }}">
                                            </div>
                                            <div class="col-xs-3">
                                                <input name="nickname" type="text" class="form-control"
                                                    placeholder="الكنية" required value="{{ old('nickname') }}">
                                            </div>
                                            <div class="col-xs-3">
                                                <input name="mothername" type="text" class="form-control"
                                                    placeholder="اسم اﻷم" required value="{{ old('mothername') }}">
                                            </div>
                                        </div>
                                        <!-- Date range -->
                                        <div class="form-group"><br>
                                            <label>تاريخ الميلاد</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input name="day" type="number" class="form-control"
                                                    placeholder="اليوم" value="{{ old('day') }}">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input name="month" type="number" class="form-control" required
                                                    placeholder="الشهر" value="{{ old('month') }}">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input name="year" type="number" class="form-control" required
                                                    placeholder="السنة" value="{{ old('year') }}">
                                            </div>
                                        </div>
                                        <!-- phone mask -->
                                        <div class="form-group">
                                            <label>الرقم الوطني</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="nationalno" type="text" class="form-control"
                                                    value=" {{ old('nationalno') }}">
                                            </div><!-- /.input group -->
                                        </div><!-- /.form group -->
                                        <div class="form-group">
                                            <label>البريد اﻹليكتروني (Gmail حصرًا)</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input name="email" type="email" class="form-control"
                                                    placeholder="email@gmail.com" value="{{ old('email') }}">
                                            </div>
                                        </div>
                                        <!-- phone mask -->
                                        <div class="form-group">
                                            <label>الهاتف</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </div>
                                                <input name="phone" type="text" class="form-control"
                                                    value="{{ old('phone') }}">
                                            </div><!-- /.input group -->
                                            <div class="form-group"><br>
                                                <label>ملاحظات لهذا الموعد</label>
                                                <textarea name="note" class="form-control" rows="3"
                                                    placeholder="اكتب ...">{{ old('note') }}</textarea>
                                                <div class="form-group"><br>
                                                    <hr>
                                                    <label>برجاء تعبئة هذا الملف ثم إعادة إرساله</label>
                                                    <a href="{{ asset('newform.pdf') }}" download>اضغط هنا
                                                        للتحميل</a>
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <input type="file" name="pdf" class="form-control">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                            </div><!-- /.box -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" style="width:100%">تسجيل الموعد</button>
                            </div>
                            </form>
                        </div>
                    </div><!-- /.row -->

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
            </div>
            <strong>{{ __('messages.copy_rights') }} </strong>
        </footer>

    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.4 -->
    <script src=" {{ asset('../../plugins/jQuery/jQuery-2.1.4.min.js') }} "></script>
    <!-- Bootstrap 3.3.4 -->
    <script src=" {{ asset('../../bootstrap/js/bootstrap.min.js') }} "></script>
    <!-- Select2 -->
    <script src=" {{ asset('../../plugins/select2/select2.full.min.js') }} "></script>
    <!-- InputMask -->
    <script src=" {{ asset('../../plugins/input-mask/jquery.inputmask.js') }} "></script>
    <script src=" {{ asset('../../plugins/input-mask/jquery.inputmask.date.extensions.js') }} "></script>
    <script src=" {{ asset('../../plugins/input-mask/jquery.inputmask.extensions.js') }} "></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src=" {{ asset('../../plugins/daterangepicker/daterangepicker.js') }} "></script>
    <!-- bootstrap color picker -->
    <script src=" {{ asset('../../plugins/colorpicker/bootstrap-colorpicker.min.js') }} "></script>
    <!-- bootstrap time picker -->
    <script src=" {{ asset('../../plugins/timepicker/bootstrap-timepicker.min.js') }} "></script>
    <!-- SlimScroll 1.3.0 -->
    <script src=" {{ asset('../../plugins/slimScroll/jquery.slimscroll.min.js') }} "></script>
    <!-- iCheck 1.0.1 -->
    <script src=" {{ asset('../../plugins/iCheck/icheck.min.js') }} "></script>
    <!-- FastClick -->
    <script src=" {{ asset('../../plugins/fastclick/fastclick.min.js') }} "></script>
    <!-- AdminLTE App -->
    <script src=" {{ asset('../../dist/js/app.min.js') }} "></script>
    <!-- AdminLTE for demo purposes -->
    <script src=" {{ asset('../../dist/js/demo.js') }} "></script>
    <!-- Page script -->
</body>

</html>
