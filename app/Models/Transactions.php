<?php

namespace App\Models;
use App\Models\User;
use App\Models\book;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sail\Console\PublishCommand;

class Transactions extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'prices',
            'transactions_id',
            'user_id',
        )->withPivot('value');

    }

    public function books()
    {
        return $this->hasMany(
        book::class,
        'transaction',
        );
    }
}
