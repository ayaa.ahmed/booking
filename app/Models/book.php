<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Transactions;

class book extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction',
        'nationalno',
        'firstname',
        'nickname',
        'fathername',
        'mothername',
        'day',
        'month',
        'year',
        'email',
        'phone',
        'note',
        'pdf',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(
            User::class,
            'user_id',
        );
    }


    public function transactions()
    {
        return $this->hasOne(
            Transactions::class,
            'transaction'
        );
    }
}
