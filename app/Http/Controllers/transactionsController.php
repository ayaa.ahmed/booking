<?php

namespace App\Http\Controllers;

use App\Models\Price;
use Illuminate\Http\Request;
use App\Models\Transactions;
use App\Models\User;

class transactionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('is_admin');
    }

    public function create(Request $request)
    {
        $trans = Transactions::All();
        $title = __('messages.create_trans');
        if($request->method()=='GET')
        {
            return view('create_trans', compact('trans', 'title'));
        }

        if($request->method()=='POST')
        {
            $valCategoryidator = $request->validate([
                'name'      => 'required',
            ]);

            Transactions::create([
                'name' => $request->name,
            ]);
            return redirect()->back()->with('success', __('messages.Transaction_created'));
        }
    }
}
