<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Transactions;
use App\DataTables\userDataTable;
use App\Models\Price;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('is_admin');
    }

    public function index(userDataTable $user)
    {
        return $user->render('users',['title'=> __('messages.admin')]);
    }

    public function create()
    {
        $transactions= Transactions::All();
        $users= User::where('id',Auth::user()->id)->with('transactions')->first();
        $title = __('messages.create_user');
        return view('create_user', compact('title','transactions'));

        /*foreach($users->transactions as $price)
            echo $price->name . "|" . $price->pivot->value."<br>";*/
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'email'         => 'required|unique:users',
            'password'      => 'required',
            'credit'        => 'required',
            'is_admin'      => 'required',
        ]);

        $users = new User();
        $users->name= request('name');
        $users->email= request('email');
        $users->password=Hash::make(request('password'));
        $users->credit=request('credit');
        $users->is_admin=request('is_admin');
        $users->save();

        $request->validate([
            'price.*'         => 'required',
        ]);
        $count = 1;
        foreach($request->price as $price)
        {
            $prices = new Price();
            $trans_id = $count++;
            $prices->user_id= $users->id;
            $prices->transactions_id = $trans_id;
            $prices->value = $price;
            $prices->save();
        }

        session()->flash('success', __('messages.user.added'));
        return redirect(url('/users'));
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = User::find($id);
        $transactions= Transactions::All();
        $users= User::where('id',$id)->with('transactions')->first();
        $title = __('messages.edit_user');

        /*foreach($users->transactions as $price)
            echo $price->id . "|" . $price->pivot->value."<br>";*/

        return view('edit_user',compact('user','title','users'));
    }

    public function update(Request $request,$id)
    {

        $request->validate([
            'name'          => 'required',
            'email'         => 'unique:users,email,'.$id,
            'password'      => 'required',
            'credit'        => 'required',
            'is_admin'      => 'required',
        ]);

        $users = User::find($id);
        $users->name= request('name');
        $users->email= request('email');
        $users->password=Hash::make(request('password'));
        $users->credit=request('credit');
        $users->is_admin=request('is_admin');
        $users->update();

        $request->validate([
            'price.*'         => 'required',
        ]);
        $count = 1;
        foreach($request->price as $price)
        {
            $trans_id = $count++;
            DB::table('prices')->where('user_id', $id)->where('transactions_id', $trans_id)->update([
                'value' => $price,
            ]);
        }

        session()->flash('success', __('messages.updated.user'));
        return redirect(url('/users'));
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users.index')
                        ->with('success',__('messages.deleted.user'));
    }

    public function multi_delete() {
		if (is_array(request('item'))) {
			User::destroy(request('item'));
		} else {
			User::find(request('item'))->delete();
		}
        return redirect()->route('users.index')
                        ->with('success',__('messages.deleted_record'));
	}

    public function status_update($id)
    {
        $user = User::find($id);

        if($user->status == '1')
        {
            DB::table('users')->where('id',$id)->update(['status' => 0]);
            return redirect()->route('users.index')
                            ->with('success',__('messages.banned.user'));
        }
        else
        {
            DB::table('users')->where('id',$id)->update(['status' => 1]);
            return redirect()->route('users.index')
                            ->with('success',__('messages.unbanned.user'));
        }
    }
}
