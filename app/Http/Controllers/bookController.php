<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\book;
use App\DataTables\bookDataTable;
use App\Models\Transactions;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class bookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(bookDataTable $admin)
    {
        return $admin->render('index',['title'=> __('messages.dashboard')]);
    }

    public function create()
    {
        $transactions= Transactions::All();
        $title = __('messages.create_book');
        return view('create_book', compact('transactions', 'title'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'transaction'       => 'required',
            'nationalno'        => 'required',
            'firstname'         => 'required',
            'nickname'          => 'required',
            'fathername'        => 'required',
            'mothername'        => 'required',
            'day'               => 'required',
            'month'             => 'required',
            'year'              => 'required',
            'email'             => 'required',
            'phone'             => 'required',
            'note'              => 'required',
            'pdf'               => 'required|mimes:pdf|max:2048',
        ]);

        if($file = $request->file('pdf'))
        {
            $name=time().'.'.$request->pdf->extension();
            if($file->move('uploads', $name))
            {
                $books = new Book();
                $books->transaction =request('transaction');
                $books->nationalno = request('nationalno');
                $books->firstname= request('firstname');
                $books->nickname= request('nickname');
                $books->fathername=request('fathername');
                $books->mothername=request('mothername');
                $books->day=request('day');
                $books->month=request('month');
                $books->year=request('year');
                $books->email=request('email');
                $books->phone=request('phone');
                $books->note=request('note');
                $books->pdf = $name;
                $books->user_id = Auth::user()->id;
                $books->save();
            }
        session()->flash('success', __('messages.book.added'));
		return redirect(url('/booking'));
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user_id = Auth::user()->id;
        if(Auth::user()->is_admin == 1){
            $book = Book::find($id);
            $tran_id = $book->transaction;
        }
        else{
            $book = Book::where('user_id', $user_id)->find($id);
            if(!$book)
            return redirect('/booking');
            else
            $tran_id = $book->transaction;
        }
        $transactions= Transactions::All();
        $trans = Transactions::where('id', $tran_id)->with('books')->first();
        $title = __('messages.edit');
        return view('edit_book',compact('book','title','trans','transactions'));
    }

    public function update(Request $request,$id)
    {

        $request->validate([
            'transaction'       => 'required',
            'nationalno'        => 'required',
            'firstname'         => 'required',
            'nickname'          => 'required',
            'fathername'        => 'required',
            'mothername'        => 'required',
            'day'               => 'required',
            'month'             => 'required',
            'year'              => 'required',
            'email'             => 'required',
            'phone'             => 'required',
            'note'              => 'required',
            'pdf'               => 'required|mimes:pdf|max:2048',
        ]);

        $book = Book::find($id);
        $book->update($request->all());
        return redirect()->route('booking.index')
                        ->with('success',__('messages.updated_record'));
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->route('booking.index')
                        ->with('success',__('messages.deleted.book'));
    }

    public function multi_delete() {
		if (is_array(request('item'))) {
			Book::destroy(request('item'));
		} else {
			Book::find(request('item'))->delete();
		}
        return redirect()->route('booking.index')
                        ->with('success',__('messages.deleted_record'));
	}
}
