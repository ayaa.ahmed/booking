<?php

namespace App\DataTables;

use App\Models\User;
use NunoMaduro\Collision\Adapters\Phpunit\Style;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder;


class userDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('checkbox', 'btns.checkbox')
            ->addColumn('action', 'btns.action_user')
            ->addColumn('is_admin', function($row){
                if($row->is_admin == '1'){
                   return '<span style="color: red"><i class="fa fa-user-secret">'.' '.__('messages.Isadmin').'</span></i>';
                }elseif($row->is_admin == '0'){
                   return '<span style="color: blue"><i class="fa fa-user">'.' '.__('messages.Notadmin').'</span></i>';
                }else{
                    return false;
                }
           })
            ->rawColumns([
                'action',
                'checkbox',
                'is_admin'
            ]);
    }

    public function query(userDataTable $model)
    {
       return user::query();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('userdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        'dom'        => 'Blfrtip',
				        'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, __('messages.all_record')]],
                        'buttons'    => [
                                [ 'text' => '<i class="fa fa-user-plus"></i> '.__('messages.create_user'), 'className' => 'btn btn-info', "action" => "function(){

                                    window.location.href = '".\URL::current()."/create';
                                }"],
                                ['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
                                ['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.__('messages.ex_csv')],
                                ['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.__('messages.ex_excel')],
                                ['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
                                ['text'   => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-danger delBtn', 'style'=>'text-algin: center'],
                            ],
                        'initComplete' => " function () {
                            this.api().columns([1,2,3,4]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",
                        'language'         => [
                            'sProcessing'     => __('messages.sProcessing'),
                            'sLengthMenu'     => __('messages.sLengthMenu'),
                            'sZeroRecords'    => __('messages.sZeroRecords'),
                            'sEmptyTable'     => __('messages.sEmptyTable'),
                            'sInfo'           => __('messages.sInfo'),
                            'sInfoEmpty'      => __('messages.sInfoEmpty'),
                            'sInfoFiltered'   => __('messages.sInfoFiltered'),
                            'sInfoPostFix'    => __('messages.sInfoPostFix'),
                            'sSearch'         => __('messages.sSearch'),
                            'sUrl'            => __('messages.sUrl'),
                            'sInfoThousands'  => __('messages.sInfoThousands'),
                            'sLoadingRecords' => __('messages.sLoadingRecords'),
                            'oPaginate'       => [
                                'sFirst'         => __('messages.sFirst'),
                                'sLast'          => __('messages.sLast'),
                                'sNext'          => __('messages.sNext'),
                                'sPrevious'      => __('messages.sPrevious'),
                            ],
                            'oAria'            => [
                                'sSortAscending'  => __('messages.sSortAscending'),
                                'sSortDescending' => __('messages.sSortDescending'),
                            ],

                        ],
                    ]);
    }

    protected function getColumns()
    {
        return
        [
            [
                'name' => 'checkbox',
                'data' => 'checkbox',
                'title'      => '<input type="checkbox" class="check_all" onclick="check_all()" />',
                'exportable'=>false,
                'printable'=>false,
                'searchable'=>false,
                'orderable'=>false,
            ],[
                'name' => 'name',
                'data' => 'name',
                'title' => __('messages.name')
            ],[
                'name' => 'is_admin',
                'data' => 'is_admin',
                'title' => __('messages.status'),
            ],[
            'name' => 'email',
            'data' => 'email',
            'title' => __("messages.email")
            ],[
                'name' => 'credit',
                'data' => 'credit',
                'title' => __("messages.credit")
                ],
            [
                'name' => 'action',
                'data' => 'action',
                'title' => __('messages.Action'),
                'exportable'=>false,
                'printable'=>false,
                'searchable'=>false,
                'orderable'=>false,
            ],
        ];
    }

    protected function filename()
    {
        return 'user__' . date('YmdHis');
    }
}
