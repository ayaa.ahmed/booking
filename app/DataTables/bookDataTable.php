<?php

namespace App\DataTables;

use App\Models\book;
use App\Models\Transactions;
use NunoMaduro\Collision\Adapters\Phpunit\Style;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\Auth;


class bookDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('checkbox', 'btns.checkbox')
            ->addColumn('full_name', function ($data) {
                return $data->firstname.' '.$data->fathername.' '.$data->nickname;
            })
           ->editColumn('full_name', function ($data) {
                return $data->firstname.' '.$data->fathername.' '.$data->nickname;
            })
            ->addColumn('action', 'btns.action')
            ->addColumn('status', function($row){
                if($row->status == '1'){
                   return '<span style="color:blue"><i class="fa fa-hourglass-half fa-1x">'.' '.__('messages.pending').'</i></span>';
                }elseif($row->status == '2'){
                   return '<span style="color:green"><i class="fa fa-check fa-1x green">'.' '.__('messages.active').'</i></span>';
                }elseif($row->status == '3'){
                    return '<span style="color:red"><i class="fa fa-stop fa-1x dred">'.' '.__('messages.inactive').'</i></span>';
                }else{
                    return false;
                }
           })
           ->addColumn('transaction', function($row){
            if($row->transaction == '1'){
               return 'منح';
            }elseif($row->transaction == '2'){
               return 'أحوال';
            }elseif($row->transaction == '3'){
                return 'وكالات';
            }elseif($row->transaction == '4'){
                return 'سند';
             }elseif($row->transaction == '5'){
                 return 'تصديق';
             }elseif($row->transaction == '6'){
                return 'تصاريح';
             }elseif($row->transaction == '7'){
                 return 'تذاكر';
             }
             else{

                return false;
            }
       })

            ->rawColumns([
                'action',
                'checkbox',
                'status',
                'transaction'
            ]);
    }

    public function query(bookDataTable $model)
    {
       // return $model->newQuery();
        $user_id = Auth::user()->id;
        $is_admin = Auth::user()->is_admin;
        if($is_admin == 1)
            return book::with('user')->select('books.*');
        else
            return book::with('user')->select('books.*')->where('user_id', $user_id);
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('bookdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    /*->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload'))*/
                    ->parameters([
                        'dom'        => 'Blfrtip',
				        'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, __('messages.all_record')]],
                        'buttons'    => [
                                [ 'text' => '<i class="fa fa-plus"></i> '.__('messages.create_book'), 'className' => 'btn btn-info', "action" => "function(){

                                    window.location.href = '".\URL::current()."/create';
                                }"],
                                ['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
                                ['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.__('messages.ex_csv')],
                                ['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.__('messages.ex_excel')],
                                ['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
                                ['text'   => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-danger delBtn', 'style'=>'text-algin: center'],
                            ],
                    /*    'initComplete' => " function () {
                            this.api().columns([1,2,3,4,5]).every(function () {
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('keyup', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                            });
                        }",*/
                        'language'         => [
                            'sProcessing'     => __('messages.sProcessing'),
                            'sLengthMenu'     => __('messages.sLengthMenu'),
                            'sZeroRecords'    => __('messages.sZeroRecords'),
                            'sEmptyTable'     => __('messages.sEmptyTable'),
                            'sInfo'           => __('messages.sInfo'),
                            'sInfoEmpty'      => __('messages.sInfoEmpty'),
                            'sInfoFiltered'   => __('messages.sInfoFiltered'),
                            'sInfoPostFix'    => __('messages.sInfoPostFix'),
                            'sSearch'         => __('messages.sSearch'),
                            'sUrl'            => __('messages.sUrl'),
                            'sInfoThousands'  => __('messages.sInfoThousands'),
                            'sLoadingRecords' => __('messages.sLoadingRecords'),
                            'oPaginate'       => [
                                'sFirst'         => __('messages.sFirst'),
                                'sLast'          => __('messages.sLast'),
                                'sNext'          => __('messages.sNext'),
                                'sPrevious'      => __('messages.sPrevious'),
                            ],
                            'oAria'            => [
                                'sSortAscending'  => __('messages.sSortAscending'),
                                'sSortDescending' => __('messages.sSortDescending'),
                            ],

                        ],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       /* return [
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('id'),
            Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];*/


        return
        [
            [
            'name' => 'checkbox',
            'data' => 'checkbox',
            'title'      => '<input type="checkbox" class="check_all" onclick="check_all()" />',
            'exportable'=>false,
            'printable'=>false,
            'searchable'=>false,
            'orderable'=>false,
            ],
            [
            'name' => 'firstname',
            'data' => 'full_name',
            'title' => __('messages.name')
            ],[
            'name' => 'transaction',
            'data' => 'transaction',
            'title' => __('messages.trans')
            ],[
                'name' => 'note',
                'data' => 'note',
                'title' => __('messages.info')
            ],[
            'name' => 'email',
            'data' => 'email',
            'title' => __("messages.email")
            ],[
                'name' => 'status',
                'data' => 'status',
                'title' => __('messages.status'),
            ],[
                    'name' => 'action',
                    'data' => 'action',
                    'title' => __('messages.Action'),
                    'exportable'=>false,
                    'printable'=>false,
                    'searchable'=>false,
                    'orderable'=>false,
                ],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'book_' . date('YmdHis');
    }
}
