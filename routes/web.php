<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;
use App\Http\Controllers\bookController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\transactionsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::resource('booking', 'bookController');
Route::resource('users', 'UserController');
Route::delete('/destroy/all','bookController@multi_delete');
Route::delete('/destroy/all/users','UserController@multi_delete');

//Auth::routes();

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
  ]);

Route::get('profile', 'ChangePasswordController@index');
Route::post('profile', 'ChangePasswordController@store')->name('change.password');

Route::get('/status-update/{id}','UserController@status_update');
Route::any('transaction/create', 'transactionsController@create')->name('create/transaction');


Route::get('/pay/', function(){

        $books = App\Models\book::get();
        foreach ($books as $book){
            if($book->status =2){
                $user_id = $book->user_id;
                $transaction = $book->transaction;
                $users= App\Models\User::where('id',$user_id)->with('transactions')->first();
                foreach($users->transactions as $price){
                    if($price->id == $transaction){
                        $amount = $price->pivot->value;
                        App\Models\User::where('id',$user_id)->decrement('credit', $amount);
                    }
                }
            }
        }
});

