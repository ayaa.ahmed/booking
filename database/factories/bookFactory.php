<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\book;

class bookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = book::class;
    public function definition()
    {
        return [
            'transaction' => $this->faker->randomElement(['منح', 'سند', 'وكالات']),
            'status'=>$this->faker->numberBetween($min = 1 , $max = 3),
            'fname' => $this->faker->firstName(),
            'nickname'=> $this->faker->firstName(),
            'lname'=> $this->faker->firstName(),
            'mname' => $this->faker->firstName(),
            'day'=> $this->faker->dayOfMonth(),
            'month'=> $this->faker->date(),
            'year'=> $this->faker->date(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->phoneNumber(),
            'note'=> $this->faker->text(20),
            'pdf'=>$this->faker->randomElement(['',''])
            ];
    }
}
